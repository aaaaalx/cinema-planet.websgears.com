<?php
spl_autoload_register(function ($class_name) {
    require_once __DIR__.DIRECTORY_SEPARATOR.'classes'.DIRECTORY_SEPARATOR.$class_name . '.php';
});

$Mobile_Detect = new Mobile_Detect;

$forcedPageVersion = null;
session_start();

if(isset($_GET['v']) && preg_match('/^(m|t|d)$/', $_GET['v'])){
    $_SESSION['page_version'] = $_GET['v'];
    $path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
    header("Location: $path",true, 301);
    exit;
}
elseif(!empty($_SESSION['page_version'])){
    $forcedPageVersion = $_SESSION['page_version'];
    unset($_SESSION['page_version']);
}

if($forcedPageVersion == 'm' || (!$forcedPageVersion && $Mobile_Detect->isMobile())){
    $path = __DIR__.DIRECTORY_SEPARATOR.'views'.DIRECTORY_SEPARATOR.'mobile.php';
    require_once $path;
}
elseif($forcedPageVersion == 't' || (!$forcedPageVersion && $Mobile_Detect->isTablet())){
    $path = __DIR__.DIRECTORY_SEPARATOR.'views'.DIRECTORY_SEPARATOR.'tablet.php';
    require_once $path;
}
else{
    $path = __DIR__.DIRECTORY_SEPARATOR.'views'.DIRECTORY_SEPARATOR.'index.php';
    require_once $path;
}