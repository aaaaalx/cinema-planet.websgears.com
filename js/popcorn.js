// module aliases
let Engine = Matter.Engine,
    Render = Matter.Render,
    World = Matter.World,
    Bodies = Matter.Bodies,
    Mouse = Matter.Mouse,
    MouseConstraint = Matter.MouseConstraint,
    Events = Matter.Events,
    Query = Matter.Query,
    Body = Matter.Body;

let objects = {};
let engine = null;
let render = null;

window.onload = function () {

    start();

    // add a function to adjust the canvas size if the screen is resized
    window.onresize = function(event) {
       // w = containerElement.offsetWidth;
       // h = containerElement.offsetHeight;
       //
       //  World.clear(engine.world);
       //  Engine.clear(engine);
        render.canvas.remove();

    };

    function start() {

        let containerElement = document.getElementById('popcorn-wrap');
        let w = containerElement.offsetWidth;
        let h = containerElement.offsetHeight;

        // create an engine
        engine = Engine.create();

        let wallDensity = 0.001;
        let wallSlop = 0.000001;
        let wallMass = 10000;
        let wallRestitution = 10000;

        World.add(engine.world, [
            // walls
            Bodies.rectangle(0, 0, 40, h*2, {
                isStatic: true,
                density: wallDensity,
                slop: wallSlop,
                mass: wallMass,
                restitution: wallRestitution,
                render: {
                    //fillStyle: 'red',
                    visible: false
                }
            }),//left
            Bodies.rectangle(0, 0, w*2, 40, {
                isStatic: true,
                density: wallDensity,
                slop: wallSlop,
                mass: wallMass,
                restitution: wallRestitution,
                render: {
                    //fillStyle: 'blue',
                    visible: false
                }
            }),//top
            Bodies.rectangle(w, 0, 40, h*2, {
                isStatic: true,
                density: wallDensity,
                slop: wallSlop,
                mass: wallMass,
                restitution: wallRestitution,
                render: {
                    //fillStyle: 'green',
                    visible: false
                }
            }),//right
            Bodies.rectangle(w, h, w*2, 40, {
                isStatic: true,
                density: wallDensity,
                slop: wallSlop,
                mass: wallMass,
                restitution: wallRestitution,
                render: {
                    //fillStyle: 'yellow',
                    visible: false
                }
            })//bottom
        ]);

        // create a renderer
        render = Render.create({
            element: document.getElementById('popcorn-wrap'),
            engine: engine,
            options:
                {
                    wireframes: false,
                    background: false
                }
        });

        render.canvas.setAttribute('id', 'popcorn');
        render.canvas.setAttribute('width', w);
        render.canvas.setAttribute('height', h);

        //--------------

        let friction = 0.5;
        let frictionStatic = 0.5;
        let density = 0.1;
        let slop = 0.0001;
        let timeScale = 1;
        let mass = 8;
        let inertia = 0;
        let motion = 10;
        let angularVelocity = 0.5;
        let speed = 10;
        let restitution = 0.05;

        //object 1
        let posX = (w * 54.54) / 100;
        objects.popcorn_1 = Bodies.circle(posX, 436, 30, {
            render: {
                sprite : {
                    texture: "img/popcorn/popcorn-big-1.png",
                }
            },
            friction: friction,
            frictionStatic: frictionStatic,
            density: density,
            slop: slop,
            timeScale: timeScale,
            mass: mass,
            inertia: inertia,
            motion: motion,
            angularVelocity: angularVelocity,
            speed: speed,
            restitution: restitution
        }, 5);

        //object 2
        posX = (w * 50.5) / 100;
        objects.popcorn_2 = Bodies.circle(posX, 461, 25, {
            render: {
                sprite : {
                    texture: "img/popcorn/popcorn-big-2.png"
                }
            },
            friction: friction,
            frictionStatic: frictionStatic,
            density: density,
            slop: slop,
            timeScale: timeScale,
            mass: mass,
            inertia: inertia,
            motion: motion,
            angularVelocity: angularVelocity,
            speed: speed,
            restitution: restitution
        }, 5);

        //object 3
        posX = (w * 55.49) / 100;
        objects.popcorn_3 = Bodies.rectangle(posX, 500, 80, 50, {
            render: {
                sprite : {
                    texture: "img/popcorn/popcorn-big-3.png"
                }
            },
            friction: friction,
            frictionStatic: frictionStatic,
            density: density,
            slop: slop,
            timeScale: timeScale,
            mass: mass,
            inertia: inertia,
            motion: motion,
            angularVelocity: angularVelocity,
            speed: speed,
            restitution: restitution
        });

        //object 4
        posX = (w * 57.9) / 100;
        objects.popcorn_4 = Bodies.rectangle(posX, 522, 80, 50, {
            render: {
                sprite : {
                    texture: "img/popcorn/popcorn-big-4.png"
                }
            },
            friction: friction,
            frictionStatic: frictionStatic,
            density: density,
            slop: slop,
            timeScale: timeScale,
            mass: mass,
            inertia: inertia,
            motion: motion,
            angularVelocity: angularVelocity,
            speed: speed,
            restitution: restitution
        });

        //object 5
        posX = (w * 64.69) / 100;
        objects.popcorn_5 = Bodies.rectangle(posX, 500, 80, 50, {
            render: {
                sprite : {
                    texture: "img/popcorn/popcorn-big-5.png"
                }
            },
            friction: friction,
            frictionStatic: frictionStatic,
            density: density,
            slop: slop,
            timeScale: timeScale,
            mass: mass,
            inertia: inertia,
            motion: motion,
            angularVelocity: angularVelocity,
            speed: speed,
            restitution: restitution
        });

        //object 6
        posX = (w * 60.69) / 100;
        objects.popcorn_6 = Bodies.rectangle(posX, 480, 80, 60, {
            render: {
                sprite : {
                    texture: "img/popcorn/popcorn-big-6.png"
                }
            },
            friction: friction,
            frictionStatic: frictionStatic,
            density: density,
            slop: slop,
            timeScale: timeScale,
            mass: mass,
            inertia: inertia,
            motion: motion,
            angularVelocity: angularVelocity,
            speed: speed,
            restitution: restitution
        });

        //object 7
        posX = (w * 68.69) / 100;
        objects.popcorn_7 = Bodies.rectangle(posX, 450, 70, 70, {
            render: {
                sprite : {
                    texture: "img/popcorn/popcorn-big-7.png"
                }
            },
            friction: friction,
            frictionStatic: frictionStatic,
            density: density,
            slop: slop,
            timeScale: timeScale,
            mass: mass,
            inertia: inertia,
            motion: motion,
            angularVelocity: angularVelocity,
            speed: speed,
            restitution: restitution
        });

        //object 8
        posX = (w * 56) / 100;
        objects.popcorn_8 = Bodies.circle(posX, 550, 25,  {
            render: {
                sprite : {
                    texture: "img/popcorn/popcorn-big-8.png"
                }
            },
            friction: friction,
            frictionStatic: frictionStatic,
            density: density,
            slop: slop,
            timeScale: timeScale,
            mass: mass,
            inertia: inertia,
            motion: motion,
            angularVelocity: angularVelocity,
            speed: speed,
            restitution: restitution
        }, 5);

        //object 9
        posX = (w * 50) / 100;
        objects.popcorn_9 = Bodies.circle(posX, 500, 30,  {
            render: {
                sprite : {
                    texture: "img/popcorn/popcorn-big-9.png"
                }
            },
            friction: friction,
            frictionStatic: frictionStatic,
            density: density,
            slop: slop,
            timeScale: timeScale,
            mass: mass,
            inertia: inertia,
            motion: motion,
            angularVelocity: angularVelocity,
            speed: speed,
            restitution: restitution
        }, 5);

        //object 10
        posX = (w * 53) / 100;
        objects.popcorn_10 = Bodies.circle(posX, 430, 45,  {
            render: {
                sprite : {
                    texture: "img/popcorn/popcorn-big-10.png"
                }
            },
            friction: friction,
            frictionStatic: frictionStatic,
            density: density,
            slop: slop,
            timeScale: timeScale,
            mass: mass,
            inertia: inertia,
            motion: motion,
            angularVelocity: angularVelocity,
            speed: speed,
            restitution: restitution
        }, 5);



        ///////////////////////////


        //object 11
        posX = (w * 54.54) / 100;
        objects.popcorn_11 = Bodies.circle(posX, 436, 30, {
            render: {
                sprite : {
                    texture: "img/popcorn/popcorn-big-1.png",
                }
            },
            friction: friction,
            frictionStatic: frictionStatic,
            density: density,
            slop: slop,
            timeScale: timeScale,
            mass: mass,
            inertia: inertia,
            motion: motion,
            angularVelocity: angularVelocity,
            speed: speed,
            restitution: restitution
        }, 5);

        //object 12
        posX = (w * 50.5) / 100;
        objects.popcorn_12 = Bodies.circle(posX, 461, 25, {
            render: {
                sprite : {
                    texture: "img/popcorn/popcorn-big-2.png"
                }
            },
            friction: friction,
            frictionStatic: frictionStatic,
            density: density,
            slop: slop,
            timeScale: timeScale,
            mass: mass,
            inertia: inertia,
            motion: motion,
            angularVelocity: angularVelocity,
            speed: speed,
            restitution: restitution
        }, 5);

        //object 13
        posX = (w * 55.49) / 100;
        objects.popcorn_13 = Bodies.rectangle(posX, 500, 80, 50, {
            render: {
                sprite : {
                    texture: "img/popcorn/popcorn-big-3.png"
                }
            },
            friction: friction,
            frictionStatic: frictionStatic,
            density: density,
            slop: slop,
            timeScale: timeScale,
            mass: mass,
            inertia: inertia,
            motion: motion,
            angularVelocity: angularVelocity,
            speed: speed,
            restitution: restitution
        });

        //object 14
        posX = (w * 57.9) / 100;
        objects.popcorn_14 = Bodies.rectangle(posX, 522, 80, 50, {
            render: {
                sprite : {
                    texture: "img/popcorn/popcorn-big-4.png"
                }
            },
            friction: friction,
            frictionStatic: frictionStatic,
            density: density,
            slop: slop,
            timeScale: timeScale,
            mass: mass,
            inertia: inertia,
            motion: motion,
            angularVelocity: angularVelocity,
            speed: speed,
            restitution: restitution
        });

        //object 15
        posX = (w * 64.69) / 100;
        objects.popcorn_15 = Bodies.rectangle(posX, 500, 80, 50, {
            render: {
                sprite : {
                    texture: "img/popcorn/popcorn-big-5.png"
                }
            },
            friction: friction,
            frictionStatic: frictionStatic,
            density: density,
            slop: slop,
            timeScale: timeScale,
            mass: mass,
            inertia: inertia,
            motion: motion,
            angularVelocity: angularVelocity,
            speed: speed,
            restitution: restitution
        });

        //object 16
        posX = (w * 60.69) / 100;
        objects.popcorn_16 = Bodies.rectangle(posX, 480, 80, 60, {
            render: {
                sprite : {
                    texture: "img/popcorn/popcorn-big-6.png"
                }
            },
            friction: friction,
            frictionStatic: frictionStatic,
            density: density,
            slop: slop,
            timeScale: timeScale,
            mass: mass,
            inertia: inertia,
            motion: motion,
            angularVelocity: angularVelocity,
            speed: speed,
            restitution: restitution
        });

        //object 17
        posX = (w * 68.69) / 100;
        objects.popcorn_17 = Bodies.rectangle(posX, 450, 70, 70, {
            render: {
                sprite : {
                    texture: "img/popcorn/popcorn-big-7.png"
                }
            },
            friction: friction,
            frictionStatic: frictionStatic,
            density: density,
            slop: slop,
            timeScale: timeScale,
            mass: mass,
            inertia: inertia,
            motion: motion,
            angularVelocity: angularVelocity,
            speed: speed,
            restitution: restitution
        });

        //object 18
        posX = (w * 56) / 100;
        objects.popcorn_18 = Bodies.circle(posX, 550, 25,  {
            render: {
                sprite : {
                    texture: "img/popcorn/popcorn-big-8.png"
                }
            },
            friction: friction,
            frictionStatic: frictionStatic,
            density: density,
            slop: slop,
            timeScale: timeScale,
            mass: mass,
            inertia: inertia,
            motion: motion,
            angularVelocity: angularVelocity,
            speed: speed,
            restitution: restitution
        }, 5);

        //object 19
        posX = (w * 50) / 100;
        objects.popcorn_19 = Bodies.circle(posX, 500, 30,  {
            render: {
                sprite : {
                    texture: "img/popcorn/popcorn-big-9.png"
                }
            },
            friction: friction,
            frictionStatic: frictionStatic,
            density: density,
            slop: slop,
            timeScale: timeScale,
            mass: mass,
            inertia: inertia,
            motion: motion,
            angularVelocity: angularVelocity,
            speed: speed,
            restitution: restitution
        }, 5);

        //object 20
        posX = (w * 53) / 100;
        objects.popcorn_20 = Bodies.circle(posX, 430, 45,  {
            render: {
                sprite : {
                    texture: "img/popcorn/popcorn-big-10.png"
                }
            },
            friction: friction,
            frictionStatic: frictionStatic,
            density: density,
            slop: slop,
            timeScale: timeScale,
            mass: mass,
            inertia: inertia,
            motion: motion,
            angularVelocity: angularVelocity,
            speed: speed,
            restitution: restitution
        }, 5);


        //----------------------

        // add mouse control
        let mouse = Mouse.create(render.canvas);

        //allow mouse scroll button above canvas
        mouse.element.removeEventListener("mousewheel", mouse.mousewheel, false);
        mouse.element.removeEventListener("DOMMouseScroll", mouse.mousewheel);

        let mouse_constraint = MouseConstraint.create(engine, {
            mouse: mouse,
            constraint: {
                // allow bodies on mouse to rotate
                angularStiffness: 10,
                length: 30,
                stiffness: 30,
                render: {
                    visible: false
                }
            }
        });

        objects.mouseBody = Bodies.circle(0, 0, 20,{
            isStatic: true,
            friction: 0.5,
            frictionAir: 0,
            frictionStatic: 0.5,
            density: 0.001,
            slop: 0.0001,
            timeScale: 1,
            chamfer: 0,
            mass: 100,
            motion: 0,
            speed: 0,
            restitution: 0.1,
            inertia: 0,
            angularVelocity: 0,
            render: {
                //fillStyle: 'red',
                visible: false
            }
        },4);

        // keep the mouse in sync with rendering
        render.mouse = mouse;

        function handleMouseMove(e) {
            let offsetX = render.canvas.getBoundingClientRect().left;
            let offsetY = render.canvas.getBoundingClientRect().top;
            let mouseX = parseInt(e.clientX - offsetX);
            let mouseY = parseInt(e.clientY - offsetY);
            return {x: mouseX, y: mouseY};
        }

        let mouseDirection = {
            x: 0,
            y: 0
        };
        function getMouseDirection(mousePosition){
            let res = {};
            if(mousePosition.x < mouseDirection.x){
                res.x = 'left';
            }
            else if(mousePosition.x > mouseDirection.x){
                res.x = 'right';
            }
            if(mousePosition.y < mouseDirection.y){
                res.y = 'top';
            }
            else if(mousePosition.y > mouseDirection.y){
                res.y = 'bottom';
            }
            mouseDirection.x = mousePosition.x;
            mouseDirection.y = mousePosition.y;
            return res;
        }

        render.canvas.addEventListener('mousemove', function (e) {
            let mousePosition = handleMouseMove(e);
            let direction = getMouseDirection(mousePosition);
            let velocityX = 0;
            let velocityY = 0;
            let value = 10;
            if(direction.x == 'left'){
                velocityX = -value;
            }
            else{
                velocityX = value;
            }
            if(direction.y == 'top'){
                velocityY = -value;
            }
            else{
                velocityY = value;
            }
            Body.setPosition(objects.mouseBody, {x: mousePosition.x, y: mousePosition.y});
            Body.setVelocity(objects.mouseBody, {x: velocityX, y: velocityY});
        });

        render.canvas.addEventListener('mouseenter', function (e) {
            World.add(engine.world, objects.mouseBody);

        });
        render.canvas.addEventListener('mouseleave', function (e) {
            World.remove(engine.world, objects.mouseBody);
        });

        World.add(engine.world, mouse_constraint);

        // add all of the bodies to the world
        World.add(engine.world, [
            objects.popcorn_1,
            objects.popcorn_2,
            objects.popcorn_3,
            objects.popcorn_4,
            objects.popcorn_5,
            objects.popcorn_6,
            objects.popcorn_7,
            objects.popcorn_8,
            objects.popcorn_9,
            objects.popcorn_10,

            objects.popcorn_11,
            objects.popcorn_12,
            objects.popcorn_13,
            objects.popcorn_14,
            objects.popcorn_15,
            objects.popcorn_16,
            objects.popcorn_17,
            objects.popcorn_18,
            objects.popcorn_19,
            objects.popcorn_20,

            objects.mouseBody
        ]);

        // run the renderer
        Render.run(render);


    }

};