var reload = true;
window.addEventListener('resize', function () {
    if(reload){
        if(PAGE_VERSION != 'm' && innerWidth < 700){
            reload = false;
            reloadPage('m');
        }
        else if(PAGE_VERSION != 't' && innerWidth >= 700 && innerWidth < 1024){
            reload = false;
            reloadPage('t');
        }
        else if(PAGE_VERSION != 'd' && window.innerWidth >= 1024){
            reload = false;
            reloadPage('d');
        }
    }
});

function reloadPage(pageVersion) {
    let url = self.location.origin+self.location.pathname;
    if(self.location.search){
        let query = self.location.search.replace('?', '').split('&');
        query.forEach(function (item, index) {
            if(index === 0){
                url += '?';
            }
            else{
                url += '&';
            }
            if(item.indexOf('v=') === 0){
                url += 'v='+pageVersion;
            }
            else{
                url += item;
            }
        });
    }
    else {
        url += '?v='+pageVersion;
    }
    window.location.href = url;
}