/*header menu toggle*/

$(document).on('click', '.menu-open', function(){
  $(".nav-bg-scroll").addClass("lg");
  $(".logo-dark").addClass("transparent");
  $(this).removeClass('menu-open').addClass("menu-close");
  $(".nav-box").toggleClass("nav-hidden");
});

$(document).on('click', '.menu-close', function(){
    $(".nav-bg-scroll").removeClass("lg");
    $(".nav-box").toggleClass("nav-hidden");
    $(".logo-dark").removeClass("transparent");
    $(this).removeClass('menu-close').addClass("menu-open");
});

/*slider-info*/
$(".slider-info").slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  fade: true,
  arrows: true,
  infinite: true,
  speed: 1000,
  autoplay: true,
  autoplaySpeed: 10000,
  nextArrow: '<div class="arrow-next next-count"></div>',
  prevArrow: '<div class="arrow-prev prev-count"></div>',

});

/*slides counter*/
var totalSlides = $(".slider-info .item:not(.slick-cloned)").length;

var pos = 1;
countSlides();

function countSlides(){
    $('.slider-info-counter').html('<div style="float:left;margin-left:80px;">0' + pos + '</div><div style="float:right;margin-right:80px;">/0' + totalSlides + '</div>');
}

$('.slider-info').on('afterChange', function(event, slick, currentSlide, nextSlide){
    pos = currentSlide+1;
    countSlides();
});

/*section-faq animation*/
$(".faq-btn-toggle").click(function(){
  $(".accordion-box").slideToggle();
  $(".faq-open").fadeToggle();
  $(".faq-close").fadeToggle();
});

/*change tab-bg-color*/
$(".printer-icon").click(function(){
  $(".offer-background-wrap .left-side").css("background-color", "#17173e");
    animateCertifications(500);
});
$(".laptop-icon").click(function(){
  $(".offer-background-wrap .left-side").css("background-color", "#3cb4e7");
    animateCertificationsElectronic(500);
});
$(".relux-icon").click(function(){
    $(".offer-background-wrap .left-side").css("background-color", "#17173e");
    animateCertifications(500);
});

/*custom input number*/
(function() {
  
  window.inputNumber = function(el) {

    var min = el.attr('min') || false;
    var max = el.attr('max') || false;

    var els = {};

    els.dec = el.prev();
    els.inc = el.next();

    el.each(function() {
      init($(this));
    });

    function init(el) {

      els.dec.on('click', decrement);
      els.inc.on('click', increment);

      function decrement() {
        var value = el[0].value;
        value--;
        if(!min || value >= min) {
          el[0].value = value;
        }
      }

      function increment() {
        var value = el[0].value;
        value++;
        if(!max || value <= max) {
          el[0].value = value++;
        }
      }
    }
  }
})();

inputNumber($('#inputCounter1'));
inputNumber($('#inputCounter2'));
inputNumber($('#inputCounter3'));

$(document).ready(function () {

    var movementStrength = 25;
    var el = $("body");
    var elBg1 = $(".body-bg-1");
    var elBg2 = $(".body-bg-2");
    var height = movementStrength / el.height();
    var width = movementStrength / el.width();
    el.on('mousemove', function(e){
        var pageX = e.pageX - (el.width() / 2);
        var pageY = e.pageY - (el.height() / 2);
        var newvalueX = width * pageX * -1 - 2;
        var newvalueY = height * pageY * -1 - 10;
        elBg2.css("background-position", newvalueX+"px "+newvalueY+"px");
        newvalueX = newvalueX*2;
        newvalueY = newvalueY*2;
        elBg1.css("background-position", newvalueX+"px "+newvalueY+"px");
    });

    var sameHeightElems = $('[data-height="sameHeight"]');
    setSameHeight(sameHeightElems);

    $('.how-to-use .icon-block').on('click', function () {
        if(runningAnimations[$(this).parent().attr('class')]){
            closeHowToUseSection(this);
        }
        else{
            openHowToUseSection(this);
        }
    });

    $('.how-to-use .icon-close-block').on('click', function () {
        closeHowToUseSection($(this).prev().prev());
    });

    /*scroll navigation*/
    $('.nav-list li a').on('click', function(e) {
        var section = $(this).attr('href').substr($(this).attr('href').indexOf('#'));
        if(!section){
            e.preventDefault();
        }
        var $section = $(section);
        if($section.length){
            $('.nav-list li').removeClass('active');
            $(this).parent().addClass('active');
            $('html, body').animate({
                scrollTop: $section.offset().top-130
            }, 1000);
        }
    });

    function setSliderArrows() {
        var arrow = $('.slick-arrow');
        var parent = arrow.parent();
        var topOffset = parent.find('.slide-info-img-box');
        if(topOffset.length){
            arrow.css('top', topOffset.height());
        }
    }

    setSliderArrows();
    $( window ).resize(function() {
        setSliderArrows();
    });


});

var runningAnimations = {};
function openHowToUseSection($this, duration) {
    duration = duration?duration:1000;
    var icon = $($this);
    var content = icon.siblings('.center-content');
    var block = icon.parent();
    var items = content.find('.block-item, .block-item-2');
    if(runningAnimations[block.attr('class')] == true){
        return;
    }
    runningAnimations[block.attr('class')] = true;
    var outerHeight = content.outerHeight(true);
    items.each(function(k,v) {
        outerHeight += $(v).outerHeight(true);
    });
    content.animate({
        height: outerHeight,
        marginBottom: 80
    },{
        duration: duration,
        complete:function () {
            items.each(function (k,v) {
                var d = (k+1)*300;
                var item = $(v);
                item.animate({
                    opacity: 1
                }, d);
            });
        }
    });

    var side = icon.parents('.left-side');
    if(side.length){
        side.find('.icon-close-block img').removeClass('hidden');
    }
    side = icon.parents('.right-side');
    if(side.length){
        side.find('.icon-close-block img').removeClass('hidden');
        $('.printed').fadeIn();
    }

}

function closeHowToUseSection($this, duration) {
    duration = duration?duration:1000;
    var icon = $($this);
    var content = icon.siblings('.center-content');
    var block = icon.parent();
    var items = content.find('.block-item, .block-item-2');
    if(runningAnimations[block.attr('class')] == false){
        return;
    }
    var d = 0;
    $(items.get().reverse()).each(function (k,v) {
        d = (k+1)*300;
        $(v).animate({
            opacity: 0
        }, d);
    });
    setTimeout(function () {
        content.animate({
            height: 0,
            marginBottom: 0
        },{
            duration: duration,
            complete:function () {
                var side = icon.parents('.left-side');
                if(side.length){
                    side.find('.icon-close-block img').addClass('hidden');
                }
                side = icon.parents('.right-side');
                if(side.length){
                    side.find('.icon-close-block img').addClass('hidden');
                    $('.printed').fadeOut();
                }
            }
        });
        runningAnimations[block.attr('class')] = false;
    }, duration);
}

function setSameHeight(elems) {
    var $elems = $(elems);
    if($elems.length){
        var height = 0;
        $elems.each(function (k,v) {
            var elHeight = $(v).height();
            if(elHeight>height){
                height = elHeight;
            }
        });
        if(height>0){
            $elems.each(function (k,v) {
                $(v).height(height);
            });
        }
    }
}

var AfterWowEffects = {
    'animationstart': {
        'section-offer': [
            'animateCertifications'
        ]
    },
    'animationend': {
        'popcorn-start': [
            'popcorn'
        ]
    }
};
function afterReveal (el) {
    el.addEventListener('animationstart', function () {
        if(el.getAttribute('id') && typeof AfterWowEffects['animationstart'][el.getAttribute('id')] !== 'undefined'){
            var events = AfterWowEffects['animationstart'][el.getAttribute('id')];
            $.each(events, function (k, v) {
                window[v]();
            });
        }
    });
    el.addEventListener('animationend', function () {
        if(el.getAttribute('id') && typeof AfterWowEffects['animationend'][el.getAttribute('id')] !== 'undefined'){
            var events = AfterWowEffects['animationend'][el.getAttribute('id')];
            $.each(events, function (k, v) {
                window[v]();
                delete AfterWowEffects['animationend'][el.getAttribute('id')][k];
            });
            //clear empty array items
            AfterWowEffects['animationend'][el.getAttribute('id')] = AfterWowEffects['animationend'][el.getAttribute('id')].filter(function (el) {
                return el;
            });
        }
    });
}

function popcorn() {
    //run the engine
    var forceLeftX = -0.04;
    var forceRightX = 0.04;
    var forceTopY = -0.06;
    var forceBottomY = 0.06;

    Engine.run(engine);
    Body.applyForce( objects.popcorn_1, {x: objects.popcorn_1.position.x, y: objects.popcorn_1.position.y}, {x: forceLeftX, y: forceTopY});
    Body.applyForce( objects.popcorn_2, {x: objects.popcorn_2.position.x, y: objects.popcorn_2.position.y}, {x: forceLeftX, y: forceTopY});
    Body.applyForce( objects.popcorn_3, {x: objects.popcorn_3.position.x, y: objects.popcorn_3.position.y}, {x: forceLeftX, y: forceTopY});
    Body.applyForce( objects.popcorn_4, {x: objects.popcorn_4.position.x, y: objects.popcorn_4.position.y}, {x: forceLeftX+0.04, y: forceTopY-0.07});
    Body.applyForce( objects.popcorn_5, {x: objects.popcorn_5.position.x, y: objects.popcorn_5.position.y}, {x: forceLeftX, y: forceTopY-0.03});

    Body.applyForce( objects.popcorn_6, {x: objects.popcorn_6.position.x, y: objects.popcorn_6.position.y}, {x: forceRightX, y: forceTopY});
    Body.applyForce( objects.popcorn_7, {x: objects.popcorn_7.position.x, y: objects.popcorn_7.position.y}, {x: forceRightX, y: forceTopY});
    Body.applyForce( objects.popcorn_8, {x: objects.popcorn_8.position.x, y: objects.popcorn_8.position.y}, {x: forceRightX, y: forceTopY});
    Body.applyForce( objects.popcorn_9, {x: objects.popcorn_9.position.x, y: objects.popcorn_9.position.y}, {x: forceRightX, y: forceTopY});
    Body.applyForce( objects.popcorn_10, {x: objects.popcorn_10.position.x, y: objects.popcorn_10.position.y}, {x: forceRightX, y: forceTopY});

    Body.applyForce( objects.popcorn_16, {x: objects.popcorn_16.position.x, y: objects.popcorn_16.position.y}, {x: forceLeftX, y: forceTopY});
    Body.applyForce( objects.popcorn_17, {x: objects.popcorn_17.position.x, y: objects.popcorn_17.position.y}, {x: forceLeftX, y: forceTopY});
    Body.applyForce( objects.popcorn_18, {x: objects.popcorn_18.position.x, y: objects.popcorn_18.position.y}, {x: forceLeftX, y: forceTopY});
    Body.applyForce( objects.popcorn_19, {x: objects.popcorn_19.position.x, y: objects.popcorn_19.position.y}, {x: forceLeftX+0.04, y: forceTopY-0.07});
    Body.applyForce( objects.popcorn_20, {x: objects.popcorn_20.position.x, y: objects.popcorn_20.position.y}, {x: forceLeftX, y: forceTopY-0.03});

    Body.applyForce( objects.popcorn_11, {x: objects.popcorn_11.position.x, y: objects.popcorn_11.position.y}, {x: forceRightX, y: forceTopY});
    Body.applyForce( objects.popcorn_12, {x: objects.popcorn_12.position.x, y: objects.popcorn_12.position.y}, {x: forceRightX, y: forceTopY});
    Body.applyForce( objects.popcorn_13, {x: objects.popcorn_13.position.x, y: objects.popcorn_13.position.y}, {x: forceRightX, y: forceTopY});
    Body.applyForce( objects.popcorn_14, {x: objects.popcorn_14.position.x, y: objects.popcorn_14.position.y}, {x: forceRightX, y: forceTopY});
    Body.applyForce( objects.popcorn_15, {x: objects.popcorn_15.position.x, y: objects.popcorn_15.position.y}, {x: forceRightX, y: forceTopY});


    //Body.setVelocity( objects.popcorn_1, {x: -10, y: -15});

    var anim = setInterval(function () {

        if(objects.popcorn_1.friction < 0.8){
            objects.popcorn_1.friction += 0.1;
        }
        else{
            clearTimeout(anim);
            engine.world.gravity.scale = 0;
        }
    }, 800);
    setActiveElementsUnderCanvas();
}

function setActiveElementsUnderCanvas() {
    var container = $('#popcorn-wrap');
    var clones = container.find('.clone-el');
    if(clones.length){
        clones.each(function (k, v) {
            var el = $(v);
            var zIndex = el.css('z-index');

            zIndex = Number.isInteger(parseInt(zIndex))?zIndex:0;
            el.css('z-index', 500+parseInt(zIndex));

        });
    }
}

function animateCertifications(duration, items) {
    duration = duration?duration:1500;
    items = items?items:$('.offer-img-box .animete-item');
    if(items.length){
        var pos = 0;
        var step = 100;
        items.each(function (k,v) {
            $(v).attr('style', '');
            $(v).animate({
                bottom: pos
            }, {
                duration: duration
            });
            pos += step;
        });
    }
}

function animateCertificationsElectronic(duration, items) {
    duration = duration?duration:1000;
    items = items?items:$('.offer-img-box-2 .animete-item-2');
    if(items.length){
        var pos = 0;
        items.each(function (k,v) {
            $(v).attr('style', '');
            $(v).animate({
                marginLeft: pos
            }, {
                duration: duration
            });
        });
    }
}


//custom select
$('select').each(function(){
    var $this = $(this), numberOfOptions = $(this).children('option').length;

    $this.addClass('select-hidden');
    $this.wrap('<div class="select"></div>');
    $this.after('<div class="select-styled"></div>');

    var $styledSelect = $this.next('div.select-styled');
    $styledSelect.text($this.children('option').eq(0).text());

    var $list = $('<ul />', {
        'class': 'select-options'
    }).insertAfter($styledSelect);

    for (var i = 0; i < numberOfOptions; i++) {
        $('<li />', {
            text: $this.children('option').eq(i).text(),
            rel: $this.children('option').eq(i).val()
        }).appendTo($list);
    }

    var $listItems = $list.children('li');

    $styledSelect.click(function(e) {
        e.stopPropagation();
        $('div.select-styled.active').not(this).each(function(){
            $(this).removeClass('active').next('ul.select-options').hide();
        });
        $(this).toggleClass('active').next('ul.select-options').toggle();
    });

    $listItems.click(function(e) {
        e.stopPropagation();
        $styledSelect.text($(this).text()).removeClass('active');
        $this.val($(this).attr('rel'));
        $list.hide();
    });

    $(document).click(function() {
        $styledSelect.removeClass('active');
        $list.hide();
    });

});