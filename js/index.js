/*header menu toggle*/

$(".menu-open").click(function(){  
  $(".nav-bg-scroll").addClass("lg");
  $(".logo-dark").addClass("transparent");
  $(".menu-social-links, .phone-box").fadeOut("fast");
  $(this).addClass("hidden");
  $(".nav-box").toggleClass("nav-hidden");
    $("header").removeClass("header-default").addClass("header-white");
});

$(".menu-close").click(function(){  
    $(".nav-bg-scroll").removeClass("lg");
    $(".menu-social-links").fadeIn("fast");
    $(".nav-box").toggleClass("nav-hidden");
    $(".logo-dark").removeClass("transparent"); 
    $(".phone-box").fadeIn("fast");
    $(".menu-open").removeClass("hidden");
    $("header").removeClass("header-white").addClass("header-default");
});


var scrolled = 0;
$(window).scroll(function(){
    setMenu();
});
function setMenu() {
    if ($(window).scrollTop() > scrolled) {
        $("header").removeClass("header-default").addClass("header-white");
    }
    else {
        $("header").removeClass("header-white").addClass("header-default");
    }
}

/*slider-info*/
$(".slider-info").slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: true,
  infinite: true,
  speed: 1000,
  autoplay: true,
  autoplaySpeed: 10000,
  nextArrow: '<div class="arrow-next next-count"></div>',
  prevArrow: '<div class="arrow-prev prev-count"></div>',

});

$('.arrow-prev').click(function(){
    console.log('prev');
});
$('.arrow-next').click(function(){
    console.log('next');
});

$('#popcorn-wrap').on('click', function (e) {

    let offsetX = this.getBoundingClientRect().left;
    let offsetY = this.getBoundingClientRect().top;
    let mouseX = parseInt(e.clientX - offsetX);
    let mouseY = parseInt(e.clientY - offsetY);

});


/*slides counter*/
var totalSlides = $(".slider-info .item").length - $(".slider-info .item.slick-cloned").length;

var pos = 1;
$('.slider-info-counter').html('<div>0' + 1 + '</div><div>/</div><div>0' + totalSlides + '</div>');

function countSlides(){
  $('.slider-info-counter').html('<div>0' + pos + '</div><div>/</div><div>0' + totalSlides + '</div>');
}

$('.slider-info').on('afterChange', function(event, slick, currentSlide, nextSlide){
    pos = currentSlide+1;
    countSlides();
});

/*section-faq animation*/
$(".faq-btn-toggle").click(function(){
  $(".accordion-box").slideToggle();
  $(".faq-open").fadeToggle();
  $(".faq-close").fadeToggle();
});

/*change tab-bg-color*/
$(".printer-icon").click(function(){
  $(".offer-background-wrap .left-side").css("background-color", "#17173e");
    animateCertifications(500);
});
$(".laptop-icon").click(function(){
  $(".offer-background-wrap .left-side").css("background-color", "#3cb4e7");
    animateCertificationsElectronic(500);
});
$(".relux-icon").click(function(){
    $(".offer-background-wrap .left-side").css("background-color", "#17173e");
    animateCertifications(500);
});

/*custom input number*/
(function() {
  
  window.inputNumber = function(el) {

    var min = el.attr('min') || false;
    var max = el.attr('max') || false;

    var els = {};

    els.dec = el.prev();
    els.inc = el.next();

    el.each(function() {
      init($(this));
    });

    function init(el) {

      els.dec.on('click', decrement);
      els.inc.on('click', increment);

      function decrement() {
        var value = el[0].value;
        value--;
        if(!min || value >= min) {
          el[0].value = value;
        }
      }

      function increment() {
        var value = el[0].value;
        value++;
        if(!max || value <= max) {
          el[0].value = value++;
        }
      }
    }
  }
})();

inputNumber($('#inputCounter1'));
inputNumber($('#inputCounter2'));
inputNumber($('#inputCounter3'));

$(document).ready(function () {
    setMenu();

    var movementStrength = 25;
    var el = $("body");
    var elBg1 = $(".body-bg-1");
    var elBg2 = $(".body-bg-2");
    var height = movementStrength / el.height();
    var width = movementStrength / el.width();
    el.on('mousemove', function(e){
        var pageX = e.pageX - (el.width() / 2);
        var pageY = e.pageY - (el.height() / 2);
        var newvalueX = width * pageX * -1 - 2;
        var newvalueY = height * pageY * -1 - 10;
        elBg2.css("background-position", newvalueX+"px "+newvalueY+"px");
        newvalueX = newvalueX*2;
        newvalueY = newvalueY*2;
        elBg1.css("background-position", newvalueX+"px "+newvalueY+"px");
    });

    var sameHeightElems = $('[data-height="sameHeight"]');
    setSameHeight(sameHeightElems);

    $('.how-to-use .icon-block').on('click', function () {
        if(isOpen_HowToUseSection){
            closeHowToUseSection(this);
        }
        else{
            openHowToUseSection(this);
        }
    });

    $('.how-to-use .icon-close-block').on('click', function () {
        closeHowToUseSection($(this).prev());
    });

    /*scroll navigation*/
    $('.nav-list li a').on('click', function(e) {
        var section = $(this).attr('href').substr($(this).attr('href').indexOf('#'));
        if(!section){
            e.preventDefault();
        }
        var $section = $(section);
        if($section.length){
            $('.nav-list li').removeClass('active');
            $(this).parent().addClass('active');
            $('html, body').animate({
                scrollTop: $section.offset().top-130
            }, 1000);
        }

    });

});

var isOpen_HowToUseSection = false;
function openHowToUseSection($this, duration) {
    isOpen_HowToUseSection = true;
    duration = duration?duration:1000;
    var icon = $($this);
    var content = icon.siblings('.center-content');
    var block = icon.parent();
    var items = content.find('.block-item, .block-item-2');
    var sibling_block = null;
    var closeButton = icon.next();
    if(block.hasClass('left-side')){
        sibling_block = block.next();

        sibling_block.animate({
            width: 0
        },{
            duration: duration/2,
            complete: function () {

                block.animate({
                    width: '100%'
                },{
                    duration: duration,
                    complete: function () {
                        items.each(function (k,v) {
                            var d = (k+1)*300;
                            var item = $(v);
                            item.css('display', 'inline-block');
                            item.animate({
                                opacity: 1
                            }, d);
                        });
                    }
                });

                icon.animate({
                    right: '100%',
                    marginRight: '-100px'
                },{
                    duration: duration
                });

                content.animate({
                    width: '100%'
                },{
                    duration: duration
                });

            }
        });

    }
    else if(block.hasClass('right-side')){
        sibling_block = block.prev();
        sibling_block.animate({
            width: 0
        },{
            duration: duration/2,
            complete: function () {

                block.animate({
                    width: '100%'
                },{
                    duration: duration,
                    complete: function () {
                        items.each(function (k,v) {
                            var d = (k+1)*300;
                            var item = $(v);
                            item.css('display', 'inline-block');
                            item.animate({
                                opacity: 1
                            }, d);
                        });
                    }
                });

                closeButton.animate({
                    right: '40',
                    marginRight: '0'
                },{
                    duration: duration+100
                });

                icon.animate({
                    left: '0'
                },{
                    duration: duration
                });

                content.animate({
                    width: '100%'
                },{
                    duration: duration
                });

                $('.printed').fadeIn();

            }
        });

    }
}

function closeHowToUseSection($this, duration) {
    isOpen_HowToUseSection = false;
    duration = duration?duration:1000;
    var icon = $($this);
    var content = icon.siblings('.center-content');
    var block = icon.parent();
    var items = content.find('.block-item, .block-item-2');
    var sibling_block = null;
    var closeButton = icon.next();
    if(block.hasClass('left-side')){
        sibling_block = block.next();

        items.animate({
            opacity: 0
        }, {
            duration: 100,
            complete: function () {
                items.css('display', 'none');
            }
        });

        block.animate({
            width: '50%'
        },{
            duration: duration/2,
            complete: function () {

                sibling_block.animate({
                    width: '50%'
                },{
                    duration: duration
                });

                icon.animate({
                    right: '20px',
                    marginRight: 0
                },{
                    duration: duration
                });

                content.animate({
                    width: 0
                },{
                    duration: duration+100
                });

            }
        });

    }
    else if(block.hasClass('right-side')){
        sibling_block = block.prev();

        items.animate({
            opacity: 0
        }, {
            duration: 100,
            complete: function () {
                items.css('display', 'none');
            }
        });

        block.animate({
            width: '50%'
        },{
            duration: duration/2,
            complete: function () {

                sibling_block.animate({
                    width: '50%'
                },{
                    duration: duration
                });

                closeButton .animate({
                    right: '100%',
                    marginRight: '-100px'
                },{
                    duration: duration
                });

                icon.animate({
                    left: '20px',
                    marginLeft: 0
                },{
                    duration: duration
                });

                content.animate({
                    width: 0
                },{
                    duration: duration+100
                });

            }
        });

        $('.printed').fadeOut();

    }
}

function setSameHeight(elems) {
    var $elems = $(elems);
    if($elems.length){
        var height = 0;
        $elems.each(function (k,v) {
            var elHeight = $(v).height();
            if(elHeight>height){
                height = elHeight;
            }
        });
        if(height>0){
            $elems.each(function (k,v) {
                $(v).height(height);
            });
        }
    }
}

var AfterWowEffects = {
    'animationstart': {
        'section-offer': [
            'animateCertifications'
        ]
    },
    'animationend': {
        'popcorn-start': [
            'popcorn'
        ]
    }
};
function afterReveal (el) {
    el.addEventListener('animationstart', function () {
        if(el.getAttribute('id') && typeof AfterWowEffects['animationstart'][el.getAttribute('id')] !== 'undefined'){
            var events = AfterWowEffects['animationstart'][el.getAttribute('id')];
            $.each(events, function (k, v) {
                window[v]();
            });
        }
    });
    el.addEventListener('animationend', function () {
        if(el.getAttribute('id') && typeof AfterWowEffects['animationend'][el.getAttribute('id')] !== 'undefined'){
            var events = AfterWowEffects['animationend'][el.getAttribute('id')];
            $.each(events, function (k, v) {
                window[v]();
                delete AfterWowEffects['animationend'][el.getAttribute('id')][k];
            });
            //clear empty array items
            AfterWowEffects['animationend'][el.getAttribute('id')] = AfterWowEffects['animationend'][el.getAttribute('id')].filter(function (el) {
                return el;
            });
        }
    });
}

function popcorn() {
    //run the engine
    var forceLeftX = -0.08;
    var forceRightX = 0.08;
    var forceTopY = -0.12;
    var forceBottomY = 0.12;

    Engine.run(engine);
    Body.applyForce( objects.popcorn_1, {x: objects.popcorn_1.position.x, y: objects.popcorn_1.position.y}, {x: forceLeftX, y: forceTopY});
    Body.applyForce( objects.popcorn_2, {x: objects.popcorn_2.position.x, y: objects.popcorn_2.position.y}, {x: forceLeftX, y: forceTopY});
    Body.applyForce( objects.popcorn_3, {x: objects.popcorn_3.position.x, y: objects.popcorn_3.position.y}, {x: forceLeftX, y: forceTopY});
    Body.applyForce( objects.popcorn_4, {x: objects.popcorn_4.position.x, y: objects.popcorn_4.position.y}, {x: forceLeftX+0.04, y: forceTopY-0.07});
    Body.applyForce( objects.popcorn_5, {x: objects.popcorn_5.position.x, y: objects.popcorn_5.position.y}, {x: forceLeftX, y: forceTopY-0.03});

    Body.applyForce( objects.popcorn_6, {x: objects.popcorn_6.position.x, y: objects.popcorn_6.position.y}, {x: forceRightX, y: forceTopY});
    Body.applyForce( objects.popcorn_7, {x: objects.popcorn_7.position.x, y: objects.popcorn_7.position.y}, {x: forceRightX, y: forceTopY});
    Body.applyForce( objects.popcorn_8, {x: objects.popcorn_8.position.x, y: objects.popcorn_8.position.y}, {x: forceRightX, y: forceTopY});
    Body.applyForce( objects.popcorn_9, {x: objects.popcorn_9.position.x, y: objects.popcorn_9.position.y}, {x: forceRightX, y: forceTopY});
    Body.applyForce( objects.popcorn_10, {x: objects.popcorn_10.position.x, y: objects.popcorn_10.position.y}, {x: forceRightX, y: forceTopY});

    Body.applyForce( objects.popcorn_16, {x: objects.popcorn_16.position.x, y: objects.popcorn_16.position.y}, {x: forceLeftX, y: forceTopY});
    Body.applyForce( objects.popcorn_17, {x: objects.popcorn_17.position.x, y: objects.popcorn_17.position.y}, {x: forceLeftX, y: forceTopY});
    Body.applyForce( objects.popcorn_18, {x: objects.popcorn_18.position.x, y: objects.popcorn_18.position.y}, {x: forceLeftX, y: forceTopY});
    Body.applyForce( objects.popcorn_19, {x: objects.popcorn_19.position.x, y: objects.popcorn_19.position.y}, {x: forceLeftX+0.04, y: forceTopY-0.07});
    Body.applyForce( objects.popcorn_20, {x: objects.popcorn_20.position.x, y: objects.popcorn_20.position.y}, {x: forceLeftX, y: forceTopY-0.03});

    Body.applyForce( objects.popcorn_11, {x: objects.popcorn_11.position.x, y: objects.popcorn_11.position.y}, {x: forceRightX, y: forceTopY});
    Body.applyForce( objects.popcorn_12, {x: objects.popcorn_12.position.x, y: objects.popcorn_12.position.y}, {x: forceRightX, y: forceTopY});
    Body.applyForce( objects.popcorn_13, {x: objects.popcorn_13.position.x, y: objects.popcorn_13.position.y}, {x: forceRightX, y: forceTopY});
    Body.applyForce( objects.popcorn_14, {x: objects.popcorn_14.position.x, y: objects.popcorn_14.position.y}, {x: forceRightX, y: forceTopY});
    Body.applyForce( objects.popcorn_15, {x: objects.popcorn_15.position.x, y: objects.popcorn_15.position.y}, {x: forceRightX, y: forceTopY});


    //Body.setVelocity( objects.popcorn_1, {x: -10, y: -15});

    setTimeout(function () {
        engine.world.gravity.scale = 0;
    }, 800);

    setActiveElementsUnderCanvas();
}

function setActiveElementsUnderCanvas() {
    var container = $('#popcorn-wrap');
    var clones = container.find('.clone-el');
    if(clones.length){
        clones.each(function (k, v) {
            var el = $(v);
            var zIndex = el.css('z-index');

            zIndex = Number.isInteger(parseInt(zIndex))?zIndex:0;
            el.css('z-index', 500+parseInt(zIndex));

        });
    }
}

function animateCertifications(duration, items) {
    duration = duration?duration:1500;
    items = items?items:$('.offer-img-box .animete-item');
    CertificationsLoop(duration, items);
}

function CertificationsLoop(duration, items) {
    if(items.length){
        var pos = 0;
        var step = 100;
        items.each(function (k,v) {
            $(v).attr('style', '');
            $(v).animate({
                bottom: pos
            }, {
                duration: duration
            });
            pos += step;
        });
    }
}

function animateCertificationsElectronic(duration, items) {
    duration = duration?duration:2000;
    items = items?items:$('.offer-img-box .animete-item-2');
    CertificationsLoop(duration, items);
}


//custom select
$('select').each(function(){
    var $this = $(this), numberOfOptions = $(this).children('option').length;

    $this.addClass('select-hidden');
    $this.wrap('<div class="select"></div>');
    $this.after('<div class="select-styled"></div>');

    var $styledSelect = $this.next('div.select-styled');
    $styledSelect.text($this.children('option').eq(0).text());

    var $list = $('<ul />', {
        'class': 'select-options'
    }).insertAfter($styledSelect);

    for (var i = 0; i < numberOfOptions; i++) {
        $('<li />', {
            text: $this.children('option').eq(i).text(),
            rel: $this.children('option').eq(i).val()
        }).appendTo($list);
    }

    var $listItems = $list.children('li');

    $styledSelect.click(function(e) {
        e.stopPropagation();
        $('div.select-styled.active').not(this).each(function(){
            $(this).removeClass('active').next('ul.select-options').hide();
        });
        $(this).toggleClass('active').next('ul.select-options').toggle();
    });

    $listItems.click(function(e) {
        e.stopPropagation();
        $styledSelect.text($(this).text()).removeClass('active');
        $this.val($(this).attr('rel'));
        $list.hide();
    });

    $(document).click(function() {
        $styledSelect.removeClass('active');
        $list.hide();
    });

});